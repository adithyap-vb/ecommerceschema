const { Double } = require("bson");

const dbName = "ecommerce";

async function cartApi(client) {
  try {
    const result = await client
      .db(dbName)
      .collection("Carts")
      .insertMany([
        {
          products: ["Kindle"],
          user: "Adithya Pantangi",
          productQty: 3,
          basePrice: Double(500.0),
          sellPrice: Double(600.0),
          total: Double(600.0),
        },
        {
          products: ["MX Master", "Kindle"],
          user: "Ellsworth Summit",
          productQty: 5,
          basePrice: Double(500.0),
          sellPrice: Double(600.0),
          total: Double(600.0),
        },

        {
          products: ["Watchmen", "Design Of Everyday Things"],
          user: "Robert Sterling",
          productQty: 1,
          basePrice: Double(44.5),
          sellPrice: Double(44.5),
          total: Double(44.5),
        },

        {
          products: ["Keychron K2"],
          user: "Taylor Sterling",
          productQty: 3,
          basePrice: Double(344.5),
          sellPrice: Double(360.0),
          total: Double(360.0),
        },

        {
          products: [],
          user: "Peter Moore",
          productQty: 0,
          basePrice: Double(0.0),
          sellPrice: Double(0.0),
          total: Double(0.0),
        },
      ]);
    console.log(
      `New carts created with the following id: ${result.insertedId}`
    );

    //Display Carts from Carts Collections
    const cursor = client.db(dbName).collection("Carts").find({});
    const results = await cursor.toArray();
    console.log(results);

    //Update Carts with total above 500
    const updateResult = await client
      .db(dbName)
      .collection("Carts")

      .updateMany(
        { total: { $gte: Double(500) } },
        { $set: { total: Double(500) } }
      );

    console.log(`${updateResult.modifiedCount} document(s) was/were updated.`);

    //Delete Carts with zero orders

    const deleteResult = await client
      .db(dbName)
      .collection("Carts")

      .deleteMany({ total: 0 });

    console.log(`${deleteResult.deletedCount} document(s) was/were deleted.`);
  } catch (err) {
    console.log(err);
  }
}

module.exports = cartApi;
