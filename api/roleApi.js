const dbName = "ecommerce";

async function roleApi(client) {
  try {
    //Insert Role Documents
    const insertResult = await client
      .db(dbName)
      .collection("Roles")
      .insertMany([
        { roleName: "Customer", roleSlug: "customer" },
        {
          roleName: "Admin",
          roleSlug: "",
        },
      ]);

    console.log(
      `New Roles created with the following id: ${insertResult.insertedId}`
    );

    //Read roles from Roles Collection
    const cursor = client.db(dbName).collection("Roles").find({});
    const results = await cursor.toArray();
    console.log(results);

    //Update Role Slug
    const updateResult = await client
      .db(dbName)
      .collection("Roles")
      .updateMany({ roleSlug: "" }, { $set: { roleSlug: "Admin" } });
    console.log(`${updateResult.modifiedCount} document(s) was/were updated.`);

    //Delete One Admin Role
    const deleteResult = await client
      .db(dbName)
      .collection("Roles")
      .deleteOne({ roleSlug: "Admin" });
    console.log(`${deleteResult.deletedCount} document(s) was/were deleted.`);
  } catch (err) {
    console.log(err);
  }
}

module.exports = roleApi;
