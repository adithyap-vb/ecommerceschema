const { Double } = require("bson");

const dbName = "ecommerce";

async function orderApi(client) {
  try {
    const insertResult = await client
      .db(dbName)
      .collection("Orders")
      .insertMany([
        {
          userIds: "userId1",
          products: ["Kindle", "Macbook Pro"],
          totalItems: 2,
          billingAddress: "Address1",
          shippingAddress: "Address1",
          transactionstatus: "Completed",
          paymentMode: "Debit Card",
          paymentStatus: "Completed",
          orderStatus: "Delivered",
        },
        {
          userIds: "userId2",
          products: ["Kindle"],
          totalItems: 1,
          billingAddress: "Address2",
          shippingAddress: "Address2",
          transactionstatus: "Completed",
          paymentMode: "Credit Card",
          paymentStatus: "Completed",
          orderStatus: "Processing",
        },
        {
          userIds: "userId3",
          products: ["Keychron K2", "MX Master Mouse"],
          totalItems: 3,
          billingAddress: "Address3",
          shippingAddress: "Address3",
          transactionstatus: "Pending",
          paymentMode: "Cash",
          paymentStatus: "Pending",
          orderStatus: "Processing",
        },

        {
          userIds: "userId4",
          products: ["MX Master Mouse "],
          totalItems: 1,
          billingAddress: "Address4",
          shippingAddress: "Address4",
          transactionstatus: "Completed",
          paymentMode: "Debit Card",
          paymentStatus: "Completed",
          orderStatus: "Delivered",
        },
        {
          userIds: "userId5",
          products: ["Design Of Everyday Things"],
          totalItems: 1,
          billingAddress: "Address5",
          shippingAddress: "Address5",
          transactionstatus: "Completed",
          paymentMode: "Debit Card",
          paymentStatus: "Completed",
          orderStatus: "Delivered",
        },
      ]);

    console.log(
      `New Orders created with the following id: ${insertResult.insertedId}`
    );

    //Read Orders from Orders Collection
    const cursor = client.db(dbName).collection("Orders").find({});
    const results = await cursor.toArray();
    console.log(results);

    //Update Pending Orders  with Transaction Status as Complete
    const updateResult = await client
      .db(dbName)
      .collection("Orders")

      .updateMany(
        { transactionstatus: "Completed" },
        { $set: { orderStatus: "Delivered" } }
      );

    console.log(`${updateResult.modifiedCount} document(s) was/were updated.`);

    //Delete Orders with paymentStatus as Pending

    const deleteResult = await client
      .db(dbName)
      .collection("Orders")

      .deleteMany({ paymentStatus: "Pending" });

    console.log(`${deleteResult.deletedCount} document(s) was/were deleted.`);
  } catch (err) {
    console.log(err);
  }
}

module.exports = orderApi;
