const dbName = "ecommerce";
async function usersApi(client) {
  try {
    // Inserts the following data to the users collection.
    const insertResult = await client
      .db(dbName)
      .collection("Users")
      .insertMany([
        {
          userFirstName: "Adithya",
          userLastName: "Pantangi",
          userEmail: "adithya@email.com",
          userProfileImage: "",
          userRole: "Customer",
        },
        {
          userFirstName: "Ellsworth",
          userLastName: "Summit",
          userEmail: "ellsworth@email.com",
          userProfileImage: "",
          userRole: "Customer",
        },
        {
          userFirstName: "Robert",
          userLastName: "Sterling",
          userEmail: "robert@email.com",
          userProfileImage: "",
          userRole: "Customer",
        },
        {
          userFirstName: "Taylor",
          userLastName: "Sterling",
          userEmail: "taylor@email.com",
          userProfileImage: "",
          userRole: "Seller",
        },
        {
          userFirstName: "Peter",
          userLastName: "Moore",
          userEmail: "peter@email.com",
          userProfileImage: "",
          userRole: "Seller",
        },
      ]);
    console.log(
      `New Users created with the following id: ${insertResult.insertedId}`
    );

    //Read Users from Users Collection
    const cursor = client.db(dbName).collection("Users").find({});
    const results = await cursor.toArray();
    //Displays each user in users collection
    console.log(results);
    await cursor.forEach((item) => {
      console.log(item);
    });

    //Update empty profile image with default profile Image
    const updateResult = await client
      .db(dbName)
      .collection("Users")
      .updateMany(
        { userProfileImage: "" },
        {
          $set: {
            userProfileImage: "profileImageUrl",
          },
        }
      );
    console.log(
      `${updateResult.matchedCount} document(s) matched the query criteria.`
    );
    console.log(`${updateResult.modifiedCount} document(s) was/were updated.`);

    //Delete seller documents
    const deleteResult = await client
      .db(dbName)
      .collection("Users")
      .deleteMany({ userRole: "Seller" });
    console.log(`${deleteResult.deletedCount} document(s) was/were deleted.`);
  } catch (err) {
    console.log(err);
  }
}
module.exports = usersApi;
