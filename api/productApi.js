const { Double } = require("bson");
const dbName = "ecommerce";

async function productsApi(client) {
  try {
    //Insert products in Products Collection

    const insertResult = client
      .db(dbName)
      .collection("Products")
      .insertMany([
        {
          productName: "Watchmen",
          productDescription: "books",
          productThumbnail: "",
          productGallery: [],
          productBasePrice: Double(600.0),
          productSellPrice: Double(400.0),
          productCategoryName: "Books",
          productTags: [],
          productAdditionalInfo: "DC Comics",
        },
        {
          productName: "Macbook Pro",
          productDescription: "M1 Macbook Pro 13 inch",
          productThumbnail: "",
          productGallery: [],
          productBasePrice: Double(130000.0),
          productSellPrice: Double(130000.0),
          productCategoryName: "Electronics",
          productTags: [],
          productAdditionalInfo: "M1 Macbook pro by Apple",
        },
        {
          productName: "Kindle",
          productDescription: "Amazon Kindle",
          productThumbnail: "",
          productGallery: [],
          productBasePrice: Double(7000.0),
          productSellPrice: Double(6000.0),
          productCategoryName: "Electronics",
          productTags: [],
          productAdditionalInfo: "Kindle By Amazon",
        },
        {
          productName: "The Design Of Everyday Things",
          productDescription: "Design Book",
          productThumbnail: "",
          productGallery: [],
          productBasePrice: Double(700.0),
          productSellPrice: Double(600.0),
          productCategoryName: "Books",
          productTags: [],
          productAdditionalInfo: "Book By Don Norman",
        },
      ]);
    console.log(
      `New Products created with the following id: ${insertResult.insertedId}`
    );

    //Read products from Products Collection
    const cursor = client.db(dbName).collection("Products").find({});
    const results = await cursor.toArray();
    //Displays each product in Products collection
    console.log(results);

    //Update empty book thumbnails
    const updateResult = await client
      .db(dbName)
      .collection("Products")
      .updateMany(
        { $and: [{ productThumbnail: "" }, { productCategoryName: "Books" }] },
        { $set: { productThumbnail: "NewThumbnail" } }
      );
    console.log(
      `${updateResult.matchedCount} document(s) matched the query criteria.`
    );
    console.log(`${updateResult.modifiedCount} document(s) was/were updated.`);

    //Delete Products from Electronics Category with sell price less than 10000
    const deleteResult = await client
      .db(dbName)
      .collection("Products")
      .deleteMany({
        $and: [
          { productCategoryName: "Electronics" },
          { productSellPrice: { $lte: 10000 } },
        ],
      });
    console.log(`${deleteResult.deletedCount} document(s) was/were deleted.`);
  } catch (err) {
    console.log(err);
  }
}

module.exports = productsApi;
