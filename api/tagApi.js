const dbName = "ecommerce";

async function tagApi(client) {
  try {
    const result = await client
      .db(dbName)
      .collection("Tags")
      .insertMany([
        { tagName: "Mobiles", tagSlug: "mobiles" },
        { tagName: "Clothes", tagSlug: "clothes" },
      ]);
    console.log(`New Tag created with the following id: ${result.insertedId}`);

    //Read Tags from Tags Collection
    const cursor = client.db(dbName).collection("Tags").find({});

    const results = await cursor.toArray();
    console.log(results);

    const updateResult = await client
      .db(dbName)
      .collection("Tags")

      .updateMany(
        { tagName: "Mobiles" },
        { $set: { tagName: "Smartphones", tagSlug: "smartphones" } }
      );

    console.log(`${updateResult.modifiedCount} document(s) was/were updated.`);

    const deleteResult = await client
      .db(dbName)
      .collection("Tags")

      .deleteMany({ tagName: "Clothes" });

    console.log(`${deleteResult.deletedCount} document(s) was/were deleted.`);
  } catch (err) {
    console.log(err);
  }
}

module.exports = tagApi;
