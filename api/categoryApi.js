const dbName = "ecommerce";

async function categoryApi(client) {
  try {
    //Insert Category documents
    const insertResult = await client
      .db(dbName)
      .collection("Categories")
      .insertMany([
        {
          categoryName: "Books",
          categorySlug: "books",
          categoryImage: "imageUrl",
          categoryDescription: "This Category consists of books",
        },
        {
          categoryName: "Consumer Electronics",
          categorySlug: "consumer-electronics",
          categoryImage: "imageUrl2",
          categoryDescription: "",
        },
        {
          categoryName: "Video Games",
          categorySlug: "video-games",
          categoryImage: "imageUrl3",
          categoryDescription: "This Category consists of video games",
        },
        {
          categoryName: "Mens Fashion",
          categorySlug: "mens-fashion",
          categoryImage: "",
          categoryDescription: "",
        },
        {
          categoryName: "womens-fashion",
          categorySlug: "womens-fashion",
          categoryImage: "",
          categoryDescription: "",
        },
      ]);
    console.log(
      `New Categories created with the following id: ${insertResult.insertedId}`
    );

    //Read Categories from Categories Collection
    const cursor = client.db(dbName).collection("Categories").find({});
    const results = await cursor.toArray();
    console.log(results);

    //Update Category Documents
    const updateResult = await client
      .db(dbName)
      .collection("Categories")
      .updateMany(
        { categoryDescription: "" },
        {
          $set: {
            categoryDescription:
              "This category's description is not available.",
          },
        }
      );

    console.log(`${updateResult.modifiedCount} document(s) was/were updated.`);

    //Delete Category with empty categoryImage
    const deleteResult = await client
      .db(dbName)
      .collection("Categories")
      .deleteMany({ categoryImage: "" });
    console.log(`${deleteResult.deletedCount} document(s) was/were deleted.`);
  } catch (err) {
    console.log(err);
  }
}

module.exports = categoryApi;
