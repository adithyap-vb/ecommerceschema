const { mongoClient, MongoAPIError, MongoClient } = require("mongodb");
const url = "mongodb://localhost:27017/ecommerce";

const usersApi = require("./api/usersApi.js");
const roleApi = require("./api/roleApi.js");
const categoryApi = require("./api/categoryApi.js");
const tagApi = require("./api/tagApi.js");
const cartApi = require("./api/cartApi.js");
const productApi = require("./api/productApi.js");
const orderApi = require("./api/orderApi.js");

async function main() {
  const client = new MongoClient(url);
  try {
    await client.connect();
    await productApi(client);
    await cartApi(client);
    await categoryApi(client);
    await orderApi(client);
    await roleApi(client);
    await tagApi(client);
    await usersApi(client);
  } catch (err) {
    console.log(err);
  } finally {
    console.log(`Client connection closed`);
    await client.close();
  }
}

main().catch(console.error);
